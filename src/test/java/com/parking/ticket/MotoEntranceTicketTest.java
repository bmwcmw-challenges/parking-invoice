package com.parking.ticket;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.ZonedDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MotoEntranceTicketTest {

    private static final float DELTA = 0.00000001f;

    private static Stream<Arguments> provideHourToMotoParkingFee() {
        return Stream.of(
                Arguments.of(1f, 0f),
                Arguments.of(3.5f, 2.5f),
                Arguments.of(5.1f, 5f)
        );
    }

    @ParameterizedTest
    @MethodSource("provideHourToMotoParkingFee")
    void shouldHaveCorrectRoundedFee(float hour, float expectedFee) {
        ZonedDateTime now = ZonedDateTime.now();
        MotoEntranceTicket motoEntrance = new MotoEntranceTicket(
                now.minusMinutes((long) (hour * CarEntranceTicket.MINUTES_PER_HOUR)));
        assertEquals(expectedFee, motoEntrance.computeRoundedFee(now), DELTA);
    }

}