package com.parking.ticket;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.ZonedDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CarEntranceTicketTest {

    private static final float DELTA = 0.00000001f;

    private static Stream<Arguments> provideHourToRoundedFee() {
        return Stream.of(
                Arguments.of(1.52f, 1.5f),
                Arguments.of(1.74f, 1.5f),
                Arguments.of(1.77f, 2f),
                Arguments.of(1.99f, 2f)
        );
    }

    private static Stream<Arguments> provideHourToFeeBetweenOneAndFourHours() {
        return Stream.of(
                Arguments.of(1.5f, 1f),
                Arguments.of(2f, 2f),
                Arguments.of(4f, 6f)
        );
    }

    private static Stream<Arguments> provideHourToFeeForMoreThanFourHours() {
        return Stream.of(
                Arguments.of(4.5f, 7.5f),
                Arguments.of(5f, 9f),
                Arguments.of(7.5f, 16.5f)
        );
    }

    @ParameterizedTest
    @ValueSource(floats = {0.5f, 1f})
    void shouldNotHaveFeeForLessThanOrEqualToOneHour(float hour) {
        assertExpectedFee(hour, 0f);
    }

    @ParameterizedTest
    @MethodSource("provideHourToRoundedFee")
    void shouldHaveCorrectRoundedFee(float hour, float expectedFee) {
        assertExpectedFee(hour, expectedFee);
    }

    @ParameterizedTest
    @MethodSource("provideHourToFeeBetweenOneAndFourHours")
    void shouldHaveCorrectFeeBetweenOneAndFourHours(float hour, float expectedFee) {
        assertExpectedFee(hour, expectedFee);
    }

    @ParameterizedTest
    @MethodSource("provideHourToFeeForMoreThanFourHours")
    void shouldHaveCorrectFeeForMoreThanFourHours(float hour, float expectedFee) {
        assertExpectedFee(hour, expectedFee);
    }

    private void assertExpectedFee(float hour, float expectedFee) {
        ZonedDateTime now = ZonedDateTime.now();
        CarEntranceTicket carEntranceTicket = new CarEntranceTicket(
                now.minusMinutes((long) (hour * CarEntranceTicket.MINUTES_PER_HOUR)));
        assertEquals(expectedFee, carEntranceTicket.computeRoundedFee(now), DELTA);
    }

}