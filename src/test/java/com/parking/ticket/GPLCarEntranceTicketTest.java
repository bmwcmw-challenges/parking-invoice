package com.parking.ticket;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.ZonedDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GPLCarEntranceTicketTest {

    private static final float DELTA = 0.00000001f;

    private static Stream<Arguments> provideHourToGplParkingFee() {
        return Stream.of(
                Arguments.of(1f, 0f),
                Arguments.of(3.5f, 5.5f),
                Arguments.of(7.48f, 18f)
        );
    }

    @ParameterizedTest
    @MethodSource("provideHourToGplParkingFee")
    void shouldHaveCorrectRoundedFee(float hour, float expectedFee) {
        ZonedDateTime now = ZonedDateTime.now();
        GPLCarEntranceTicket carEntrance = new GPLCarEntranceTicket(
                now.minusMinutes((long) (hour * CarEntranceTicket.MINUTES_PER_HOUR)));
        assertEquals(expectedFee, carEntrance.computeRoundedFee(now), DELTA);
    }

}