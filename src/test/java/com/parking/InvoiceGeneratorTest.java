package com.parking;

import com.parking.ticket.CarEntranceTicket;
import com.parking.ticket.GPLCarEntranceTicket;
import com.parking.ticket.MotoEntranceTicket;
import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InvoiceGeneratorTest {

    @Test
    void shouldGenerateCorrectInvoiceForCar() {
        ZonedDateTime entranceDateTime =
                ZonedDateTime.of(2019, 1, 1,
                        13, 24, 0, 0, ZoneId.systemDefault());
        CarEntranceTicket carEntranceTicket = new CarEntranceTicket(entranceDateTime);

        ZonedDateTime exitDateTime =
                ZonedDateTime.of(2019, 1, 1,
                        15, 10, 0, 0, ZoneId.systemDefault());

        String generatedInvoice = InvoiceGenerator.generateInvoiceOnExit(carEntranceTicket, exitDateTime);
        assertNotNull(generatedInvoice);
        assertTrue(generatedInvoice.contains("voiture essence"));
        assertTrue(generatedInvoice.contains("1h46"));
        assertTrue(generatedInvoice.contains("2 euros"));
    }

    @Test
    void shouldGenerateCorrectInvoiceForMoto() {
        ZonedDateTime entranceDateTime =
                ZonedDateTime.of(2019, 1, 1,
                        19, 30, 0, 0, ZoneId.systemDefault());
        MotoEntranceTicket motoEntrance = new MotoEntranceTicket(entranceDateTime);

        ZonedDateTime exitDateTime =
                ZonedDateTime.of(2019, 1, 2,
                        0, 37, 0, 0, ZoneId.systemDefault());

        String generatedInvoice = InvoiceGenerator.generateInvoiceOnExit(motoEntrance, exitDateTime);
        assertNotNull(generatedInvoice);
        assertTrue(generatedInvoice.contains("moto essence"));
        assertTrue(generatedInvoice.contains("5h07"));
        assertTrue(generatedInvoice.contains("5 euros"));
    }

    @Test
    void shouldGenerateCorrectInvoiceForGPLCar() {
        ZonedDateTime entranceDateTime =
                ZonedDateTime.of(2019, 1, 1,
                        7, 43, 0, 0, ZoneId.systemDefault());
        GPLCarEntranceTicket carEntrance = new GPLCarEntranceTicket(entranceDateTime);

        ZonedDateTime exitDateTime =
                ZonedDateTime.of(2019, 1, 1,
                        15, 10, 0, 0, ZoneId.systemDefault());

        String generatedInvoice = InvoiceGenerator.generateInvoiceOnExit(carEntrance, exitDateTime);
        assertNotNull(generatedInvoice);
        assertTrue(generatedInvoice.contains("voiture GPL"));
        assertTrue(generatedInvoice.contains("7h27"));
        assertTrue(generatedInvoice.contains("17,5 euros"));
    }
}