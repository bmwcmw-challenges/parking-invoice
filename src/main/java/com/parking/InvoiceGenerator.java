package com.parking;

import com.parking.ticket.CarEntranceTicket;

import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.util.Locale;

class InvoiceGenerator {
    private final static String INVOICE_MODEL =
            "- véhicule : %s\n" +
            "- temps passé : %s\n" +
            "- montant dû : %s euros";
    private final static DecimalFormat FR_DECIMAL_FORMAT = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.FRANCE);

    static String generateInvoiceOnExit(CarEntranceTicket carEntranceTicket, ZonedDateTime exitDateTime) {
        long parkingDurationMinutes = carEntranceTicket.getParkingDuration(exitDateTime).toMinutes();
        return String.format(INVOICE_MODEL,
                carEntranceTicket.getType(),
                toParkingDurationString(parkingDurationMinutes), toParkingFeeString(carEntranceTicket, exitDateTime));
    }

    private static String toParkingFeeString(CarEntranceTicket carEntranceTicket, ZonedDateTime exitDateTime) {
        return FR_DECIMAL_FORMAT.format(carEntranceTicket.computeRoundedFee(exitDateTime));
    }

    private static String toParkingDurationString(long parkingDurationMinutes) {
        long hours = parkingDurationMinutes / CarEntranceTicket.MINUTES_PER_HOUR;
        long minutes = parkingDurationMinutes % CarEntranceTicket.MINUTES_PER_HOUR;
        return String.format("%dh%02d", hours, minutes);
    }
}
