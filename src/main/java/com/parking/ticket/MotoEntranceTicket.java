package com.parking.ticket;

import java.time.ZonedDateTime;

public class MotoEntranceTicket extends CarEntranceTicket {

    private static final int MOTO_FEE_DISCOUNT_RATE = 2;

    public MotoEntranceTicket(ZonedDateTime entranceDateTime) {
        super(entranceDateTime);
    }

    @Override
    protected float getExactStandardFee(long durationMinutes) {
        return super.getExactStandardFee(durationMinutes) / MOTO_FEE_DISCOUNT_RATE;
    }

    @Override
    public String getType() {
        return "moto essence";
    }
}
