package com.parking.ticket;

import java.time.Duration;
import java.time.ZonedDateTime;

public class CarEntranceTicket {
    private static final int MINUTES_PER_HALF_HOUR = 30;
    public static final int MINUTES_PER_HOUR = 60;
    private static final int MINUTES_PER_FOUR_HOURS = 240;
    private static final float FEE_PER_HOUR_FROM_ONE_TO_FOUR_HOURS = 2f;
    private static final float FEE_PER_HALF_HOUR_AFTER_FOUR_HOURS = 1.5f;
    private static final float FEE_OF_FOUR_HOURS = 6f;

    private final ZonedDateTime entranceDateTime;

    public CarEntranceTicket(ZonedDateTime entranceDateTime) {
        this.entranceDateTime = entranceDateTime;
    }

    public final float computeRoundedFee(ZonedDateTime exitTime) {
        long durationMinutes = getParkingDuration(exitTime).toMinutes();
        if (durationMinutes <= MINUTES_PER_HOUR) {
            return 0f;
        }

        float exactFee = getExactStandardFee(durationMinutes);
        return (float) (Math.ceil(exactFee * 2) / 2); // Rounded by each 0.5 to upper bound
    }

    public final Duration getParkingDuration(ZonedDateTime exitTime) {
        return Duration.between(entranceDateTime, exitTime);
    }

    /**
     * Calculates the exact parking fee for standard car type
     *
     * @param durationMinutes parking duration in minutes
     * @return non rounded fee
     */
    protected float getExactStandardFee(long durationMinutes) {
        float exactFee;
        if (durationMinutes <= MINUTES_PER_FOUR_HOURS) {
            float minutesAfterFirstHour = (durationMinutes - MINUTES_PER_HOUR);
            exactFee = FEE_PER_HOUR_FROM_ONE_TO_FOUR_HOURS * minutesAfterFirstHour / (float) MINUTES_PER_HOUR;
        } else {
            float minutesAfterFourHours = (durationMinutes - MINUTES_PER_FOUR_HOURS);
            exactFee = FEE_OF_FOUR_HOURS + FEE_PER_HALF_HOUR_AFTER_FOUR_HOURS * minutesAfterFourHours / (float) MINUTES_PER_HALF_HOUR;
        }
        return exactFee;
    }

    public String getType() {
        return "voiture essence";
    }
}