package com.parking.ticket;

import java.time.ZonedDateTime;

public class GPLCarEntranceTicket extends CarEntranceTicket {

    private static final float GPL_CAR_FEE_OVERHEAD_RATE = 1.07f;

    public GPLCarEntranceTicket(ZonedDateTime entranceDateTime) {
        super(entranceDateTime);
    }

    @Override
    protected float getExactStandardFee(long durationMinutes) {
        return super.getExactStandardFee(durationMinutes) * GPL_CAR_FEE_OVERHEAD_RATE;
    }

    @Override
    public String getType() {
        return "voiture GPL";
    }
}
